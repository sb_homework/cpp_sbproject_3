#include <iostream>
#include <cmath>
using namespace std;

void FindOddNumbers(int N, int x)
{
    for (int i = x; i <= N; i += 2)
        cout << i << " ";

    cout << "\n";
}

int main()
{
    int N;
    cout << "Your number = "; cin >> N;

    cout << "Even: \n";
    FindOddNumbers(N, 0);

    cout << "Odd: \n";
    FindOddNumbers(N, 1);

    system("pause");
    return 0;
}